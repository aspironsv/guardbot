// 'use strict';
const dao = require('./base');


const addCommand = command =>
	Command.update(
		{ name: command.name },
		Object.assign({}, command, { isActive: false }),
		{ upsert: true }
	);
	await addCommand({ id, name: newCommand, state: 'role' });
//	{"id":369616959,"name":"kimdir","state":null,"isActive":true,"_id":"ahZwUEaJF6eLssmm","role":"everyone","content":"So'rash uchun so'ramang, so'ramoqchi bo'lgan narsangizni darrov so'rang.","type":"text"}
const addCommandpg = command =>
	dao.knex('commands')
	.instert({
	id: command.id,
	name: command.name,
	isActive: false,
	state: command.state,
	thisKeyIsSkipped: undefined
	});


const updateCommand = (data) =>
	Command.update({ id: data.id, isActive: false }, { $set: data });

const removeCommand = command => Command.remove(command);

const getCommand = (data) => Command.findOne(data);

const listCommands = () =>
	Command.cfind({ isActive: true }).sort({ name: 1 }).exec();

module.exports = {
	addCommand,
	getCommand,
	listCommands,
	removeCommand,
	updateCommand,
};
