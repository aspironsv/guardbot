

// Utils
const { logError } = require('../utils/log');
const Datastore = require('nedb-promise');

const Restricts = new Datastore({
	autoload: true,
	filename: 'data/Restricts.db'
});

const addUser = ({ id, groupid }) =>
	Restricts.insert(
		{ id, status: 'restricted', groupid }
	);

const isUserRestricted = ({ id, groupid }) =>
	Restricts.findOne({ id, groupid });

const getUser = user =>
	Restricts.findOne(user);

const changeStatus = ({ id, groupid }) => {
	User.update(
		{ id },
		{ $set: { status: 'activated' } },
		{ groupid },
		{ returnUpdatedDocs: true, upsert: true }
	).then(getUpdatedDocument);
};

module.exports = {
	addUser,
	changeStatus,
	isUserRestricted
};
