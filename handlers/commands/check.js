'use strict';

// Bot
//const { replyOptions } = require('../../bot/options');

// DB
//const { getUser } = require('../../stores/user');

// Actions
const check = require('../../actions/check');

const checkHandler = async (ctx) => {
	const { message, replyWithHTML} = ctx;
	const { from } = message.from;

	if (message.chat.type === 'private') {
		
		//replyWithHTML(message.from.id);
		return check( message.from).then(replyWithHTML);
	}
	else{
		return null;
	}
};

module.exports = checkHandler;
