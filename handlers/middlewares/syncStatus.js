'use strict';
const { quietLink, scheduleDeletion } = require('../../utils/tg');
const { getUser } = require('../../stores/user');
const {addUser, isUserRestricted} = require('../../stores/restricts');
const syncStatusHandler = (ctx, next) => {
	const { message, replyWithHTML, chat } = ctx;
	//const { id } = chat;
	const { new_chat_members } = message;
	
	new_chat_members.forEach(async newMember => {
		if (newMember.is_bot) {
			return null;
		}

		const dbUser = await getUser({ id: newMember.id });

		// if user is not in DB, he can't be banned.
		if (dbUser === null || dbUser.status === 'member') {
			replyWithHTML(`${quietLink(newMember)}`+" Ushbu guruhda siz xabar qoldirolmaysiz," +
				"bu cheklovni olinishini istasangiz menga:" +
				"@sampleofbot shaxsiy xabar yozing."
			).then(scheduleDeletion);
			console.log(isUserRestricted({id: newMember.id, groupid:chat.id }));
			return ctx.restrictChatMember(newMember.id, {
				can_read_messages: true,
			}).then(addUser({id: newMember.id, groupid:chat.id }));
		
			
		}

		switch (dbUser.status) {
			case 'admin':
				return ctx.promoteChatMember(dbUser.id, {
					can_change_info: false,
					can_delete_messages: true,
					can_invite_users: true,
					can_pin_messages: true,
					can_promote_members: false,
					can_restrict_members: true,
				});
			case 'banned':
				return ctx.kickChatMember(dbUser.id);
			case 'member':
			replyWithHTML(`${quietLink(newMember)}`+" Ushbu guruhda siz xabar qoldirolmaysiz," +
					"bu cheklovni olinishini istasangiz menga:" +
					"@sampleofbot shaxsiy xabar yozing."
				).then(scheduleDeletion);
				return ctx.restrictChatMember(newMember.id, {
					can_read_messages: true,
				}).then(addUser({id: newMember.id, groupid:chat.id }));
			default:
				throw new Error(`Unexpected member status: ${dbUser.status}`);
		}
	});

	return next();
};

module.exports = syncStatusHandler;
